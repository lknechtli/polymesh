#ifndef MESH_H
#define MESH_H
 
 
#include "Face.h"
#include "Vector3d.h"
#include <vector>


class Shape;

#include "Shape.h"
#include "Normal.h"

using namespace std;

#define PI 3.1415
// ############### Mesh ######################
class Mesh{
   private:
	int numVerts;		// number of vertices in the mesh
	int numNorms;		// number of normal vertices for the mesh
	int numFaces;		// number of faces in the mesh

//	Vector3d *pt;		// array of 3D vertices
//	Vector3d *norm;		// array of normals
//	Face *face;			// array of face data
	vector <Vector3d> pt;
	vector <Normal> norm;
	vector <Face> face;

	Shape *shape;
						// others to be added later
   public:
	Mesh();			// constructor
	Mesh(Shape *shape); // constructor
	~Mesh();			// destructor
	void draw();
	void makeSurfaceMesh();
	void calcNewell();
	void calcGourand();

	void readfile(string filename);
};

#endif