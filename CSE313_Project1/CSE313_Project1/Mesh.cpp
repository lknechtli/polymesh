#include "stdafx.h"
#include "Mesh.h"
 
#include <windows.h>
#include <gl/Gl.h>
#include <gl/Glu.h>
#include <gl/glut.h>
#include <iostream>
#include <fstream>
#include <string>
  
using namespace std;


Mesh::Mesh()
{
	shape = new Shape();

}


Mesh::Mesh(Shape *shape2)
{
	shape = shape2;
}



Mesh::~Mesh()
{

}



// Make the mesh: vertex list, face list, normal list.
void Mesh::makeSurfaceMesh()
{


	// ...  Extra Credits:
	// instead of reading an object file, make a tesselated mesh surface (e.g. cylinder, cone) 
	// by yourself using tesselation here.

	// the following line is a place holder, just to initialize the window to display a cube.
	readfile("cube.obj");

/*	///////////////////////////////////////////////////////////
	// the commented code below fills in the content of the three lists (face, vertex, normal) 
	// for drawing a cube.

	numVerts = 8;
	numFaces = 6;
	numNorms = 8;

	pt.resize(numVerts);
	face.resize(numFaces);
	norm.resize(numNorms);

	// vertices
	pt[0].set( -0.5f, -0.5f, -0.5f );
	pt[1].set( -0.5f, -0.5f,  0.5f );
	pt[2].set( -0.5f,  0.5f,  0.5f );
	pt[3].set( -0.5f,  0.5f, -0.5f );
	pt[4].set( 0.5f, -0.5f, -0.5f );
	pt[5].set( 0.5f, -0.5f,  0.5f );
	pt[6].set( 0.5f,  0.5f,  0.5f );
	pt[7].set( 0.5f,  0.5f, -0.5f );
	
	// faces
	for(int i=0; i<face.size(); i++){
		face[i].vert.resize(4);
	//	face[i].vert = new VertexID[4];  // 4 vertices per face in the case of a cube
		face[i].nVerts = 4;
	}

	face[0].vert[0].vertIndex = 0;
	face[0].vert[0].normIndex = 0;
	face[0].vert[1].vertIndex = 1;
	face[0].vert[1].normIndex = 1;
	face[0].vert[2].vertIndex = 2;
	face[0].vert[2].normIndex = 2;
	face[0].vert[3].vertIndex = 3;
	face[0].vert[3].normIndex = 3;

	face[1].vert[0].vertIndex = 4;
	face[1].vert[0].normIndex = 4;
	face[1].vert[1].vertIndex = 5;
	face[1].vert[1].normIndex = 5;
	face[1].vert[2].vertIndex = 1;
	face[1].vert[2].normIndex = 1;
	face[1].vert[3].vertIndex = 0;
	face[1].vert[3].normIndex = 0;

	face[2].vert[0].vertIndex = 1;
	face[2].vert[0].normIndex = 1;
	face[2].vert[1].vertIndex = 5;
	face[2].vert[1].normIndex = 5;
	face[2].vert[2].vertIndex = 6;
	face[2].vert[2].normIndex = 6;
	face[2].vert[3].vertIndex = 2;
	face[2].vert[3].normIndex = 2;

	face[3].vert[0].vertIndex = 6;
	face[3].vert[0].normIndex = 6;
	face[3].vert[1].vertIndex = 7;
	face[3].vert[1].normIndex = 7;
	face[3].vert[2].vertIndex = 3;
	face[3].vert[2].normIndex = 3;
	face[3].vert[3].vertIndex = 2;
	face[3].vert[3].normIndex = 2;

	face[4].vert[0].vertIndex = 3;
	face[4].vert[0].normIndex = 3;
	face[4].vert[1].vertIndex = 7;
	face[4].vert[1].normIndex = 7;
	face[4].vert[2].vertIndex = 4;
	face[4].vert[2].normIndex = 4;
	face[4].vert[3].vertIndex = 0;
	face[4].vert[3].normIndex = 0;

	face[5].vert[0].vertIndex = 7;
	face[5].vert[0].normIndex = 7;
	face[5].vert[1].vertIndex = 6;
	face[5].vert[1].normIndex = 6;
	face[5].vert[2].vertIndex = 5;
	face[5].vert[2].normIndex = 5;
	face[5].vert[3].vertIndex = 4;
	face[5].vert[3].normIndex = 4;

	
	// calculate face and vertex normals
	calcNewell();
	calcGourand();

	//////////////////////////////////////////////////////
*/

}



void Mesh::draw() // use openGL to draw this mesh
{

	for(unsigned int f = 0;f <face.size();f++) // for each face
	{
		glColor3f(1,1,1);
		if(!shape->getWire())  // draw in wireframe or solid mode?
		{
			glEnable(GL_LIGHTING);
			glEnable(GL_LIGHT0);
			glBegin(GL_POLYGON);
		}
		else
		{
			glDisable(GL_LIGHTING);
			glBegin(GL_LINE_LOOP);
		}
		for(int v=0;v<face[f].nVerts;v++) // for each vertex in this face
		{	
			int in = face[f].vert[v].normIndex; // index of this normal
			int iv = face[f].vert[v].vertIndex; // index of this vertex
			if(shape->getSmooth())
				glNormal3f(norm[in].norm.x, norm[in].norm.y, norm[in].norm.z);
			else
				glNormal3f(face[f].faceNorm.norm.x, face[f].faceNorm.norm.y, face[f].faceNorm.norm.z);

			glVertex3f(pt[iv].x, pt[iv].y, pt[iv].z);
		
		}
		glEnd();

	}


	if(shape->getNormals())  // drawing normal is enabled
	{
		// .... If flat shading, draw each face normal using a short line originating from center of the face
		if(!(shape->getSmooth())){
			//calculate list of face centerpoints
			//add all points in face, divide by numpoints.
			vector<Vector3d> centerpoints;
			for(vector<Face>::iterator it = face.begin(); it != face.end(); it++){
				Vector3d current;
				for(vector<VertexID>::iterator id = it->vert.begin(); id != it->vert.end(); id++){
					current.add(pt[id->vertIndex]);
				}
				int size = it->nVerts;
				current.x /= size;
				current.y /= size;
				current.z /= size;
				centerpoints.push_back(current);
			}
			//render each normal by creating two points and a line between them (face centerpoint, and face centerpoint + normal vector)
			for(int i = 0 ; i < numFaces; i++){
				glBegin(GL_LINE_LOOP);
				glVertex3d(centerpoints[i].x,centerpoints[i].y,centerpoints[i].z);
				//centerpoints[i].add(face[i].faceNorm.norm);
				centerpoints[i].x += face[i].faceNorm.norm.x * 0.2;
				centerpoints[i].y += face[i].faceNorm.norm.y * 0.2;
				centerpoints[i].z += face[i].faceNorm.norm.z * 0.2;
				glVertex3d(centerpoints[i].x,centerpoints[i].y,centerpoints[i].z);
				glEnd();
			}
		}
		// .... If smooth shading, draw each vertex normal using a short line originating from the vertex
		else{
			for(int i = 0; i < numFaces; i++){
				for(vector<VertexID>::iterator id = face[i].vert.begin(); id != face[i].vert.end(); id++){
					Vector3d vect = pt[id->vertIndex];
					Vector3d normal = norm[id->normIndex].norm;
					glBegin(GL_LINE_LOOP);
					glVertex3d(vect.x,vect.y,vect.z);
					glVertex3d(vect.x+normal.x*.2, vect.y + normal.y*.2, vect.z + normal.z*.2);
					glEnd();
				}
			}
		}
	}
	

}


// Calculate Newell Face Normal 
void Mesh::calcNewell()
{
	// ... Use Newell's method to calculate every face normal
	for(vector<Face>::iterator it = face.begin(); it!=face.end(); it++){
		Normal currNorm;
		Face * const currFace = &*it;
		Vector3d const * current;
		Vector3d const * next;
		for(int i = 0; i < currFace->nVerts; i++){ 
			Vector3d add;
			Vector3d subtract;
			Vector3d multiply;
			current = &pt[currFace->vert[i].vertIndex];
			next = &pt[currFace->vert[(i+1)%currFace->nVerts].vertIndex];
			subtract = add = *current;
			add.add(*next);
			subtract.sub(*next);
			/*add.x *= subtract.x;
			add.y *= subtract.y;
			add.z *= subtract.z;*/
			currNorm.norm.x += add.z * subtract.y;
			currNorm.norm.y += add.x * subtract.z;
			currNorm.norm.z += add.y * subtract.x;
		}
		currNorm.norm.normalize();
		currFace->faceNorm = currNorm;
	}
}


//Calculate Normal at Vertices
void Mesh::calcGourand()
{
	// ... Calculate vertex normals for the purpose of smooth shading
	//iterate through all faces, and for each point, add to face normal to that points normal.
	//finally, normalize all vectors
	numNorms = numVerts;
	norm.reserve(numNorms);
	for(int i = 0; i < numNorms; i++){
		norm.push_back(Normal());
	}
	for(int i = 0; i < numFaces; i++){
		for(vector<VertexID>::iterator it = face[i].vert.begin(); it != face[i].vert.end(); it++){
			it->normIndex=it->vertIndex;
			norm[it->normIndex].norm.add(face[i].faceNorm.norm);
		}
	}
	for(vector<Normal>::iterator it = norm.begin(); it != norm.end(); it++){
		it->norm.normalize();
	}
	
}


void Mesh::readfile(string filename)
{
	// read a mesh from a Wavefront OBJ object model file.
	// fill in the content of the three lists

	ifstream in(filename.c_str());
	string s;
	double x,y,z;
	int vindex;

	Vector3d v;
	Face f;
	VertexID vid;

	norm.resize(0);
	face.resize(0);
	pt.resize(0);

	in >> s;
	while(1)
	{
		if(s=="v")
		{
			in >> x >> y >> z;
			v.set(x,y,z);
			pt.push_back(v);
			if(!(in >> s))
				break;

		}
		else if(s=="f")
		{

			while((in >> s) && s[0]>='0' && s[0]<='9')
			{
	
				vindex = atoi(s.c_str());
	
				vid.vertIndex = vid.normIndex = vindex-1;
				f.vert.push_back(vid);
				f.nVerts=f.vert.size();
			}
			face.push_back(f);
			f.vert.resize(0);
		}
		else
		{
			getline(in,s);
			if(!(in>>s))
				break;
		}
	} 

	norm.resize(pt.size());

	numVerts = pt.size();
	numFaces = face.size();
	numNorms = norm.size();

	// call the functions to calculate face normals and vertex normals
	calcNewell();
	calcGourand();


	in.close();
}

// Extra credits: write a function for output an OBJ model file. For instance, you can make a surface 
// mesh using tesselation and then output the surface in an OBJ file.
