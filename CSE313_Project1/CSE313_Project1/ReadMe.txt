========================================================================
    CONSOLE APPLICATION : CSE313_Project1 Project Overview
========================================================================

Keys:
 
q: quits the program
r: enable rotation
t: translate the object
x: rotate around the X axis
y: rotate around the Y axis
z: rotate around the Z axis
+: increase size of object
-: decrease size of object
s: mouse scale mode
f: toggle smooth/flat shading
n: enable / display normal vectors
p: Toggle wireframe / polygon mode
C: initialize a new cube
o: open an object file, give full name (including .obj) at prompt

Mouse: Mouse drag to move, rotate, or zoom.

At any time, press r, then x, y, or z to switch from translation mode to rotation mode.
Press t to go back to translate mode.
 